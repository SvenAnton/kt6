import java.io.*;
import java.util.*;

public class GraphTask {

   private static BufferedReader bufferCSV(String filePath) throws FileNotFoundException {
      File file = new File(filePath);
      return new BufferedReader(new FileReader(file));
   }


   private static void checkCSV(String row, String filePath, int counter) {
      String[] commaSplit = row.split(",");
      if (commaSplit.length < 2 || (!commaSplit[1].startsWith("\"") && commaSplit.length > 2)) {
         throw new IllegalArgumentException(String.format("" +
                         "\n\nThe row '%s' in file '%s'(row nr %s) is invalid. \nProbably there is a missing" +
                         "comma or missing links list. \nPlease note that" +
                         "even empty list has to be present as an argument.\n",
                 row, filePath, counter));
      }
   }

   private static LinkedList<String> createStack(String row) {
      return new LinkedList<> (Arrays.asList(
              row.replaceAll("\"", "").split(",")));
   }

   public static Graph createPageGraph(String filePath) throws IOException {
      Graph pageLink = new Graph("PageLink");
      BufferedReader br = bufferCSV(filePath);
      String row;
      int counter = 0;

      while ((row = br.readLine()) != null) {
         if (++counter == 1) { continue;}
         checkCSV(row, filePath, counter);

         LinkedList<String> rowStack = createStack(row);
         Vertex origin = pageLink.setVertex(rowStack.pop());
         Vertex target = pageLink.setVertex(rowStack.pop());
         Arc link = pageLink.initializeLink(origin, target);
         origin.setFirst(link);

         if (rowStack.size() > 0) pageLink.setArcs(rowStack, origin, link);
      }
      return pageLink;
   }

   /** Main method. */
   public static void main (String[] args) throws IOException {
      Graph pageGraph = createPageGraph("testData/goodExample.txt");
      System.out.println(pageGraph);
   }
} 


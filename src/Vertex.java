public class Vertex {

    private String id;
    private Vertex next;
    private Arc first;
    private int info = 0;


    public String getId() { return id; }
    public Vertex getNext() { return next; }
    public void setNext(Vertex next) { this.next = next; }
    public Arc getFirst() { return first; }
    public void setFirst(Arc first) { this.first = first; }
    public int getInfo() { return info; }
    public void setInfo(int info) { this.info = info; }

    Vertex (String s) { this.id = s; }

    @Override
    public String toString() {
        return id;
    }

}


public class Arc {
    private String id;
    private Vertex target;
    private Arc next;

    public Vertex getTarget() { return target; }
    public void setTarget(Vertex target) { this.target = target; }
    public Arc getNext() { return next; }
    public void setNext(Arc next) { this.next = next; }

    Arc (String s) {
        this.id = s;
    }

    @Override
    public String toString() {
        return id;
    }
}

import static org.junit.Assert.*;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;

public class GraphTaskTest {

   private final Graph goodGraph = GraphTask.createPageGraph("testData/goodExample.txt");

   public GraphTaskTest() throws IOException {
   }


   @Test (timeout=2000)
   public void numberOfUniquePages() {
      int number = 0;
      Vertex firstVertex = goodGraph.getFirst();
      while (firstVertex != null)
      {
         number++;
         firstVertex = firstVertex.getNext();
      }
      assertEquals (7, number);
   }

   @Test (timeout=2000)
   public void numberOfLinks() {
      int number = 0;
      Vertex firstVertex = goodGraph.getFirst();
      while (firstVertex != null)
      {
         Arc firstArc = firstVertex.getFirst();
         while (firstArc != null)
         {
            number++;
            firstArc = firstArc.getNext();
         }
         firstVertex = firstVertex.getNext();
      }
      assertEquals (11, number);
   }

   @Test (timeout=2000)
   public void testArc() {
      Vertex testPage = goodGraph.getVertexById("taltech.ee");
      Arc testLink = testPage.getFirst();
      String testString = "epood.ee,sven.ee,kool.ee,tehnika.ee";
      LinkedList<String> testValues = new LinkedList<>(Arrays.asList(testString.split(",")));

      while (testLink != null) {
         String t = testLink.getTarget().getId();
         assertTrue(testValues.contains(t));
         testLink = testLink.getNext();
      }
   }

   @Test (expected=RuntimeException.class)
   public void noLinksGraph() throws IOException {
      GraphTask.createPageGraph("testData/noLinks.txt");
   }

   @Test (expected=RuntimeException.class)
   public void noLinksGraph2() throws IOException {
      GraphTask.createPageGraph("testData/noLinks.txt");
   }

}

